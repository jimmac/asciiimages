# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the asciiimages package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: asciiimages\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-09 21:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: App Name: Please translate as "ASCII Images" unless you have good reason not to.
#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:4
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:7 src/window.ui:38
msgid "ASCII Images"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:5
msgid "Convert images into ASCII characters"
msgstr ""

#. Translators: These are search terms for the application;
#. the translation should contain the original list AND the translated strings.
#. The list must end with a semicolon.
#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:14
msgid "image;ascii;convert;conversion;text;"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:8 src/main.py:113
msgid "ASCII Images Contributors"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:9
msgid "Turn images into text"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:12
msgid ""
"ASCII images uses jp2a to convert images into ASCII characters. You can save "
"the output to a file, copy it, and even change its resolution!"
msgstr ""

#: src/main.py:117
msgid "Copyright © 2023 ASCII Images Contributors"
msgstr ""

#: src/main.py:129
msgid "Code and Design Borrowed from"
msgstr ""

#. Translators: Do not translate "{basename(file_path)}"
#: src/window.py:74
msgid "\"{basename(file_path)}\" is not a valid image."
msgstr ""

#: src/window.py:120
msgid "Output copied to clipboard"
msgstr ""

#. Translators: Do not translate "{display_name}"
#: src/window.py:157
#, python-brace-format
msgid "Unable to save \"{display_name}\""
msgstr ""

#. Translators: Do not translate "{display_name}"
#: src/window.py:160
#, python-brace-format
msgid "\"{display_name}\" saved"
msgstr ""

#: src/window.py:161
msgid "Open"
msgstr ""

#: src/window.ui:39
msgid "Drag and drop images here"
msgstr ""

#: src/window.ui:50
msgid "Open File…"
msgstr ""

#: src/window.ui:69
msgid "Drop Here to Open"
msgstr ""

#: src/window.ui:127
msgid "Output Width"
msgstr ""

#: src/window.ui:128
msgid "Width of the ASCII image in characters"
msgstr ""

#: src/window.ui:148
msgid "Save to file"
msgstr ""

#: src/window.ui:156
msgid "Copy to clipboard"
msgstr ""

#: src/window.ui:175
msgid "_Open…"
msgstr ""

#: src/window.ui:181
msgid "_Keyboard Shortcuts"
msgstr ""

#: src/window.ui:185
msgid "_About ASCII Images"
msgstr ""
